const express = require('express');         // Express Web Server
const busboy = require('connect-busboy');   // Middleware to handle the file upload https://github.com/mscdex/connect-busboy
const path = require('path');               // Used for manipulation with path
const fs = require('fs-extra');             // Classic fs 
var multer = require('multer');
var upload = multer({ dest: 'uploads/' });
var bodyParser = require('body-parser')

const app = express(); // Initialize the express web server
app.use(busboy({
    highWaterMark: 2 * 1024 * 1024, // Set 2MiB buffer
})); // Insert the busboy middle-ware

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

const uploadPath = path.join(__dirname, 'fu/'); // Register the upload path
fs.ensureDir(uploadPath); // Make sure that he upload path exits

/**
 * Create route /upload which handles the post request
 */
app.route('/upload').post((req, res, next) => {
    req.pipe(req.busboy); // Pipe it trough busboy

    let formData = new Map();
    req.busboy.on('field', (fieldname, val) => {
        formData.set(fieldname, val);
        console.log(formData)
    });


    req.busboy.on('file', (fieldname, file, filename) => {
        filename = filename.replace(new RegExp("[^A-Za-z.0-9_-]", "gi"), '_')
        console.log(`Upload of '${filename}' started`);

        let fstream = null
        if (formData.get('filePath')) {
            // Create a write stream of the new file
            let fullPath = path.join(uploadPath, formData.get('filePath') + '/')
            fs.ensureDir(fullPath);
            fstream = fs.createWriteStream(path.join(fullPath, filename));
        } else {
            fstream = fs.createWriteStream(path.join(uploadPath, filename));
        }

        // Pipe it trough
        file.pipe(fstream);

        // On finish of the upload
        fstream.on('close', () => {
            console.log(`Upload of '${filename}' finished`);
        });
    });
    // res.redirect('/');
});


/**
 * Serve the basic index.html with upload form
 */
app.route('/').get((req, res) => {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write('<form action="upload" method="post" enctype="multipart/form-data">');
    res.write('<input type="text" name="filePath" ><br>');
    res.write('<input type="file" name="fileToUpload" multiple><br>');
    res.write('<input type="submit">');
    res.write('</form>');
    return res.end();
});

const server = app.listen(3200, function () {
    console.log(`Listening on port ${server.address().port}`);
});